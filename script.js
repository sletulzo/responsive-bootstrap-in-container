// Responsive container
function ResponsiveContainer() {
    var breakpoint = [540, 720, 960, 1140];
    var breakassoc = { 540: "xs", 720: "sm", 960: "md", 1140: "lg" };
      
    $('.col-responsive').each(function () {
        var item = $(this);
        var row = item.parent('.row-responsive');
        var size = row.width();

        // 1 - Calcul grid point
        var point = breakpoint[0];
        var ecartMin = Math.abs(size - point);
        var ecart;

        for (var i = 1; i < breakpoint.length; i++) {
            ecart = Math.abs(size - breakpoint[i]);
            if (ecart < ecartMin) {
                point = breakpoint[i];
                ecartMin = ecart;
            }
        }

        var grid = breakassoc[point];
        var gridclass = 'col-' + grid + '-';

        // 2 - Find class col
        var classes = item.attr('class').split(' ');
        var finalClass = "";

        for (var j = 0; j < classes.length; j++) {
            var iClass = classes[j];
            if (classes[j].indexOf(gridclass) != -1) {
                finalClass = classes[j];
            }
        }

        // 3 - Process width of class
        if (finalClass != "") {
            var indexNum = finalClass.lastIndexOf('-') + 1;
            var numClass = finalClass.substring(indexNum, finalClass.length);
            var newWidth = numClass / 12 * 100;
            item.css('width', newWidth + "%");
        } else {
            item.css('width', '');
        }
    });
}